# RESTful HTTP service for performing power management tasks
#
# (C) 2023 by Harald Welte <laforge@osmocom.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import icmplib

from osmocom.lpmgd.model import AvailabilityChecker


class IcmpAvailChecker(AvailabilityChecker):
    """An AvailabilityChecker implementation for determining host availability via ICMP Ping."""
    def __init__(self, dest_addr: str):
        self.dest_addr = dest_addr

    def is_available(self) -> bool:
        host = icmplib.ping(self.dest_addr, count=1, timeout=2, privileged=False)
        return host.is_alive
