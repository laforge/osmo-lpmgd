# (C) 2023 by Harald Welte <laforge@osmocom.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from osmocom.lpmgd.model import Switcher, SwitcherGroup

class DummySwitcher(Switcher):
    def __init__(self, group: SwitcherGroup, name: str, conf):
        super().__init__(group, name)
        self.conf = conf

    def _status_change(self, new_status: str):
        printf("DummySwitcher %s: Status change %s -> %s" % (self.name, self.status, new_status))

    def _obtain_actual_status(self):
        """Our dummy switcher is always off in the initial state"""
        if self.status == "unknown":
            return "off"
