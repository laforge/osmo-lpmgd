#!/usr/bin/env python3

# RESTful HTTP service for performing power management tasks
#
# (C) 2023 by Harald Welte <laforge@osmocom.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import argparse

from klein import Klein

from osmocom.lpmgd.model import Resource, SwitcherGroup
from osmocom.lpmgd.rest_server import PwrMgmtRestServer
from osmocom.lpmgd.switcher_dummy import DummySwitcher
from osmocom.lpmgd.avail_always import AlwaysAvailChecker


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("-H", "--host", help="Host/IP to bind HTTP to", default="localhost")
    parser.add_argument("-p", "--port", help="TCP port to bind HTTP to", default=9996)

    args = parser.parse_args()

    # TODO: implement this via some kind of config file

    resources = []

    swgrp1 = SwitcherGroup("swgrp1")
    switcher1 = DummySwitcher(swgrp1, "dumsw1", None)
    resources.append(Resource("resrc1", switcher=switcher1, checker=AlwaysAvailChecker()))

    prs = PwrMgmtRestServer(resources)
    prs.app.run(args.host, args.port)

if __name__ == "__main__":
    main(sys.argv)
