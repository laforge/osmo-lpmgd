osmo-lpmgd - Osmocom Lab Power Management Daemon
================================================

In our test/lab setups, we do have a number of systems that run 24/7 but
which are really used only very few hours per day. This has become very
visible after we started (a few months ago) to deploy
tasmota/influxdb/grafana for plotting many different power rails.

Direct on/off switching from within a given test job only works if that
test job is the only user of the given resource (such as e.g. a BTS in
osmo-gsm-tester).

For jenkins builders, OBS workers and similar machines, there could be
any number of concurrent users. So there's no single job that can power
on the resourec before using it, and power it off after it terminates.

What we need is a system that maintains a usage count, similar to how we
do usage/reference counting in data structures in software development.

osmo-lpmgd is a modular python daemon which offers a REST API over which
users can obtain usage tokens for named resources. The daemon then keeps
track of the current use count and switches resources on/off as needed.

Jenkins jobs would then (e.g. in a pipeline) first obtain a usage token
(which would implicitly power up the resource if it is not aleady
powwered), and release the usage token after they're gone. This way we
can power up build machines only when needed, saving significant
electrical power, reducing noise and minimizing heat dissipation.

Git Repository
--------------

You can clone from the official Osmocom  git repository using
```
git clone https://gitea.osmocom.org/laforge/osmo-lpmgd.git
```
There is a web interface at <https://gitea.osmocom.org/laforge/osmo-lpmgd>.

Installation
------------

TBD


Usage
-----

TBD.

There's the actual daemon/server program `osmo-lpmgd` as well as a
python library for an REST API client in the `osmocom.lpmgd.client`
python module.  A reference command line tool using that client is
available in `osmo-lpmgd-apitool`
